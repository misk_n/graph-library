//
// Created by nicolas on 9/21/18.
//

#ifndef PROJECT_CPU_GRAPH_HPP
#define PROJECT_CPU_GRAPH_HPP

#include "graph.hpp"

namespace gr_lib::cpu
{
    class cpu_graph final : public interface::graph
    {
        using super_t = graph;

    public:
        void build(size_t n, size_t e, const edge_iterator_t& start, const edge_iterator_t& end) override;
        ~cpu_graph() override;

        std::pair<const node_t*, const node_t*> get_adjacent_nodes(node_t n) const override;
        explicit operator string() const override;

        void compile() const override;
        void load_compiled(const std::string& path) override;

    private:
        node_t *offsets_ = nullptr;
        node_t *targets_ = nullptr;
    };

}

#endif //PROJECT_CPU_GRAPH_HPP

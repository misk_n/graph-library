//
// Created by nicolas on 9/21/18.
//

#ifndef PROJECT_GRAPH_HPP
#define PROJECT_GRAPH_HPP

#include <exception>

#include "gr-lib-types.hpp"
#include "utils/graph-builder.hpp"

namespace gr_lib::interface
{
    class graph {

    public:
        struct serialization_header
        {
            size_t nb_nodes_;
            size_t nb_edges_;
        };

    public:
        virtual void build(size_t n, size_t e, const edge_iterator_t&, const edge_iterator_t&)
        {
            built_ = true;
            nb_edges_ = e;
            nb_nodes_ = n;
        }

        virtual ~graph() = default;

        inline bool is_built() const { return built_; };
        virtual inline std::pair<const node_t*, const node_t*> get_adjacent_nodes(node_t n) const = 0;
        virtual explicit operator string() const = 0;
        virtual void compile() const = 0;
        virtual void load_compiled(const std::string& path) { built_ = true; };

        inline size_t get_nb_nodes() const
        { return nb_nodes_; }
        inline size_t get_nb_egdes() const
        { return nb_edges_; }


    protected:
        inline void built_or_failure() const
        {
            if (!built_)
                throw std::runtime_error("Call to an operation on a graph that isn't built.");
        }

    protected:
        bool built_ = false;
        size_t nb_nodes_;
        size_t nb_edges_;
    };
}

#endif //PROJECT_GRAPH_HPP

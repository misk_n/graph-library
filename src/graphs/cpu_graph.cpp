//
// Created by nicolas on 9/21/18.
//

#include <iostream>
#include "cpu_graph.hpp"

namespace gr_lib::cpu
{
    static void print(const cpu_graph& g)
    {
        std::cout << (string)g << std::endl;
    }

    void cpu_graph::build(size_t n, size_t e, const edge_iterator_t& start, const edge_iterator_t& end)
    {
        super_t::build(n, e, start, end);
        delete[] offsets_;
        delete[] targets_;
        offsets_ = new int[nb_nodes_ + 1];
        offsets_[nb_nodes_] = nb_edges_;
        targets_ = new int[nb_edges_];

        node_t current_node = -1;
        node_t offset = 0;
        for (auto it = start; it != end; it++)
        {
            while (current_node != it->first)
            {
                current_node++;
                offsets_[current_node] = offset;
            }
            targets_[offset] = it->second;
            offset++;
        }
    }

    cpu_graph::~cpu_graph()
    {
        delete[] offsets_;
        delete[] targets_;
    }

    inline std::pair<const node_t *, const node_t *> cpu_graph::get_adjacent_nodes(node_t n) const
    {
        return std::make_pair(targets_ + offsets_[n], targets_ + offsets_[n + 1]);
    }

    cpu_graph::operator string() const
    {
        string res = "CPU graph:\n\t" +
                     std::to_string(nb_nodes_) +
                     " nodes\n\t" +
                     std::to_string(nb_edges_) +
                     " edges\n\t" +
                     "CSR representation:\n\t\t";
        res += "offsets: [";
        for (size_t i = 0; i + 1< nb_nodes_; i++)
            res += std::to_string(offsets_[i]) + ", ";
        if (nb_nodes_ != 0)
            res += std::to_string(offsets_[nb_nodes_ - 1]);
        res += "]\n\t\t";
        res += "targets: [";
        for (size_t i = 0; i + 1 < nb_edges_; i++)
            res += std::to_string(targets_[i]) + ", ";
        if (nb_edges_ != 0)
            res += std::to_string(targets_[nb_edges_ - 1]);
        res += "]";
        return res;

    }

    void cpu_graph::compile() const
    {

    }

    void cpu_graph::load_compiled(const std::string &path)
    {
        graph::load_compiled(path);
    }

}

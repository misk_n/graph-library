//
// Created by nicolas on 9/26/18.
//

#ifndef PROJECT_SERIAL_BFS_HPP
#define PROJECT_SERIAL_BFS_HPP

#include "gr-lib-types.hpp"
#include "graphs/cpu_graph.hpp"
#include <queue>

namespace gr_lib::algorithms
{
    class serial_bfs
    {
    public:
        explicit serial_bfs(const cpu::cpu_graph& g);
        ~serial_bfs();
        size_t run(node_t start=0);

    private:
        void reset_dist();

    private:
        const cpu::cpu_graph& graph_;
        node_t *dist_ = nullptr;
    };

    serial_bfs::serial_bfs(const cpu::cpu_graph &g)
    : graph_(g)
    {
        dist_ = new node_t[g.get_nb_nodes()];
    }

    serial_bfs::~serial_bfs()
    {
        delete[] dist_;
    }

    size_t serial_bfs::run(node_t start)
    {
        reset_dist();
        std::queue<node_t> queue;
        queue.push(start);
        queue.push(-1);
        size_t max_dist = 0;
        dist_[start] = 0;
        while(!queue.empty())
        {
            node_t n = queue.front();
            queue.pop();
            if (n != -1)
            {
                dist_[n] = static_cast<node_t>(max_dist);
                auto adj_list = graph_.get_adjacent_nodes(n);
                for (auto it = adj_list.first; it != adj_list.second; it++)
                    if (dist_[*it] == -1)
                        queue.push(*it);
            }
            else if(!queue.empty())
            {
                max_dist++;
                queue.push(-1);
            }
        }
        return max_dist;
    }

    void serial_bfs::reset_dist()
    {
        for (size_t i = 0; i < graph_.get_nb_nodes(); i++)
            dist_[i] = -1;
    }
}

#endif //PROJECT_SERIAL_BFS_HPP

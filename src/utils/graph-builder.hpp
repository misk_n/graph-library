//
// Created by nicolas on 9/21/18.
//

#ifndef GRAPH_LIBRARY_GRAPH_BUILDER_HPP
#define GRAPH_LIBRARY_GRAPH_BUILDER_HPP

#include <algorithm>
#include <string>
#include <istream>
#include <vector>
#include <graphs/cpu_graph.hpp>

#include "gr-lib-types.hpp"

namespace gr_lib::utils
{
    template<class graph_t>
    graph_t build_matrix_market_graph(std::istream &in)
    {
        unsigned node_number = 0;
        in >> node_number;
        string devnull;
        node_t start = -1, end = -1;
        unsigned sum_node_degrees = 0;
        unsigned node_id = 0, deg = 0;
        for (size_t i = 0; i < node_number + 1; i++)
        {
            in >> node_id;
            in >> deg;
            sum_node_degrees += deg;
        }
        edge_vector_t v;
        v.reserve(sum_node_degrees / 2);
        for (size_t i = 0; true; i++)
        {
            in >> start;
            in >> end;
            if (!in.good())
                break;
            v.emplace_back(start, end);
        }
        std::sort(v.begin(), v.end());
        graph_t g;
        g.build(node_number, v.size(), v.begin(), v.end());
        return g;
    }

    template<typename graph_t>
    graph_t build_from_sorted_edges(std::istream &in)
    {
        edge_vector_t v;
        unsigned start = 0, end = 0;
        while (true)
        {
            in >> start;
            in >> end;
            if (!in.good())
                break;
            v.emplace_back(start, end);
        }
        graph_t g;
        g.build(start, v.size(), v.begin(), v.end());
        return g;
    }

}

#endif //GRAPH_LIBRARY_GRAPH_BUILDER_HPP

//
// Created by nicolas on 9/21/18.
//

#include <fstream>
#include <iostream>
#include "algorithms/serial_bfs.hpp"
#include "gr-lib-includes.hpp"

int main()
{
    using namespace gr_lib;

    {
        std::ifstream input("./scc.mtx");
        auto g = utils::build_matrix_market_graph<gr_lib::cpu::cpu_graph>(input);
        auto a = gr_lib::algorithms::serial_bfs(g);
        auto res = a.run();
        std::cout << "eccentricity of node 0: " << res << std::endl;
    }

    {
        std::ifstream input("./wiki_talk.simple_sorted");
        auto g = utils::build_from_sorted_edges<gr_lib::cpu::cpu_graph>(input);
        auto a = gr_lib::algorithms::serial_bfs(g);
        auto res = a.run();
        std::cout << "eccentricity of node 0: " << res << std::endl;
    }

    return 0;
}
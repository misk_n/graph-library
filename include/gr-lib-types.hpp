//
// Created by nicolas on 9/21/18.
//

#ifndef GRAPH_LIBRARY_GR_LIB_TYPES_HPP
#define GRAPH_LIBRARY_GR_LIB_TYPES_HPP

#include <cstddef>
#include <utility>
#include <vector>
#include <string>

namespace gr_lib
{
    using std::size_t;
    using std::string;
    using node_t = int;
    using edge_t = std::pair<node_t, node_t>;
    using edge_vector_t = std::vector<edge_t>;
    using edge_iterator_t = edge_vector_t::iterator;
}

#endif //GRAPH_LIBRARY_GR_LIB_TYPES_HPP
